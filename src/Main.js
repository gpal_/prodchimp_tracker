import React, { useEffect } from "react";
import LogIn from "./LogIn";
import App from "./App";
import { HashRouter, Route, Switch } from "react-router-dom";
import "./index.css";

function Main() {
  
  
  return (
    <>
      <HashRouter>

      <Switch>
        <Route exact path="/" component={LogIn} />
        <Route path="/app" component={App} />
      </Switch>
      </HashRouter>
      {/* <HashRouter basename="/">
        <Switch>
        <Route path="/" component={LogIn} />
        <Route path="/app" component={App} />
        </Switch>
      </HashRouter> */}
    </>
  );
}

export default Main;
