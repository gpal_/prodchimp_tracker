// import { mainServerAppUrl } from "./../../apis/mainapi";
// import { setCookiesData } from "./model/auth.service.data.model";

// export const iss = {
//   login: mainServerAppUrl + "/users/login",
//   signup: mainServerAppUrl + "/organizations/final-Submit",
//   googleAuth: mainServerAppUrl + "/organizations/googleauth",
// };

export const setCookie = (data) => {
  let expires = "";
  if (data.days) {
    let date = new Date();
    date.setTime(date.getTime() + data.days * 24 * 60 * 60 * 1000);
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = data.name + "=" + (data.value || "") + expires + "; path=/";
};
export const getCookie = (name) => {
  var nameEQ = name + "=";
  var ca = document.cookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
};
export const eraseCookie = (name) => {
  document.cookie = name + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
};

const deCodePayload = (token) => {
  return JSON.parse(atob(token));
};
export const getPayload = (token) => {
  const payload = token.split(".")[1];
  return deCodePayload(payload);
};

export const getAuthUser = () => {
  const token = get();
  if (token) {
    const payload = getPayload(token);
    return {
      _id: payload._id,
      reportsTo: payload.reportsTo,
      role: payload.role,
      emailId: payload.email,
      organization: payload?.organization,
    };
  }
};

const isValid = () => {
  const token = get();

  if (token) {
    const payload = getPayload(token);

    if (Date.now() >= payload?.exp * 1000) {
      return false;
    } else {
      return true;
    }
  } else {
    return false;
  }
};
export const isLoggedIn = () => {
  return isValid();
};




export const handle = (token) => {
  set(token);
}

export const set = (token) => {
  localStorage.setItem("token", token);
}
export const get = () => {
  return localStorage.getItem("token");
}

export const remove = () => {
  localStorage.removeItem("token");
}
