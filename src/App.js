import logo from "./logo.svg";
import "./App.css";
import { takeScreenShot } from "./helper/screen-shots";
import { useEffect, useState } from "react";
import {
  eraseCookie,
  get,
  getAuthUser,
  getCookie,
  isLoggedIn,
  remove,
} from "./authHelpers";
const { ipcRenderer } = window.require("electron");
import { io } from "socket.io-client";
import { useHistory } from "react-router-dom";
import axios from "axios";
let mainAppUrl = "http://localhost:9000/api"


function App() {
  const history = useHistory();

  let [programs, setPrograms] = useState({});
  let [configdata, setConfigdata] = useState("");
  let [isUserActive, setIsUserActive] = useState("");
  let [mySockets, setMySockets] = useState(null)
  let [appSet, setApps] = useState([]);
  const [frequency, setFrequency] = useState();
  const [isCancelModal, setCancelModal] = useState(false);
  const [allFunctions,setAllFunctions] = useState(null)

  const [isAppRunning, setAppRunning] = useState(isLoggedIn());

  const [appInterval,setAppInterval] = useState(null);
  const [ssInterval,setSsInterval] = useState(null);

  console.log({ ssInterval, appInterval, allFunctions });

  function appConfigSettingCall() {
    axios
      .get(mainAppUrl + "/trackerapp/user-configs", {
        headers: {
          Authorization: get(),
        },
      })
      .then((res) => {
        let AppData = res.data.config;
        if (AppData.screen_tracking === true) {
        let ssInterVal = setInterval(() => {            
            takeSS();
          }, Number(AppData.ss_frequency));
          setSsInterval(ssInterVal)
        }
        if (AppData.app_tracking === true) {
          let data = {
            userToken: get(),
            appTracking: AppData.app_tracking,
          };
        let appInterval =  setInterval(() => {
            trackapps(data);
          }, 5000);

          setAppInterval(appInterval)

        }
      });
  }

  useEffect(() => {
    if (isLoggedIn()) {
      appConfigSettingCall();
      
    }
  }, [null]);

  useEffect(() => {
    if (isLoggedIn()) {
      history.push("/app");
    } else {
      history.push("/");
    }
    let data = {
      userToken: get(),
    };
    ipcRenderer.send("take-system-info", data);
  }, [null]);

  useEffect(() => {

    ipcRenderer.on("update-chek",(dataOne,dataTwo) => {
      console.log({ dataOne, dataTwo });
    });
    ipcRenderer.on("update-chek-err", (dataErrOne, dataTwoErr) => {
      console.log({ dataErrOne, dataTwoErr });
    });
    if (isLoggedIn()) {
      getAppCollectionData()
    }
  }, [null])

  const getAppCollectionData = async () => {
    try {
      let data = {
        token: get(),
        organization: getAuthUser().organization
      };
      ipcRenderer.send("call-sync-app-collection", data);

    } catch (error) {
      console.log({ error });

    }
  }

  const takeSS = () => {
    let data = {
      userToken: get(),
    };

    ipcRenderer.send("take-ss", data);
  };

  const trackapps = (data) => {
    ipcRenderer.send("track-app", data);
  };

  // console.log(configdata.config.ss_frequency)
  // const ss_freq= configdata.config.ss_frequency

  // function take ss after every 5 sec.

  // useEffect(() => {
  //   setInterval(() => {
  //     if (isLoggedIn()) {
  //       takeSS()
  //       console.log("ss taken");
  //     }
  //   }, 60000)
  // },)

  // function to track app data in every sec
  // useEffect(() => {
  //     setInterval(() => {
  //       if (isLoggedIn()) {

  //       }
  //     }, 1000)
  // },[null])

  useEffect(() => {
    // if (isLoggedIn()) {
    //   ipcRenderer.send("stop-all", true);
    // }
  }, [])

  useEffect(() => {
    if (isLoggedIn()) {

      const socket = io("http://localhost:9000", {
        query: {
          token: get(),
        },
      });

      socket.on("connect", () => {
        if (socket.connected) {
          socket.on(`change-password-${getAuthUser()._id}`, (arg) => {
            remove();
            history.replace("/");
          });
          socket.on(`delete-user-${getAuthUser()._id}`, (arg) => {
            remove();
            history.replace("/");
          });
          socket.on(
            `change-settings-${getAuthUser()._id}`,
            (arg) => { 
                appConfigSettingCall()
            }
          );
        }
      });
    }
  }, []);

  ipcRenderer.on("system-resume",() => {
    console.log("system-resume");
    setAllFunctions(true)
  });
  
  ipcRenderer.on("system-suspend", () => {
    setAllFunctions(false);
  });

  useEffect(() => {
    if (isLoggedIn()) {
      ipcRenderer.on("active-window-check", (event, progress) => {
        let windowData = progress?.data;
        let data = { appData: windowData };
        axios
          .post(`${mainAppUrl}/app-useage/store`, data, {
            headers: {
              Authorization: get(),
            },
          })
          .then((res) => {
            let selectedData = res.data.createAppUsage;
            ipcRenderer.send("delete-app-data", selectedData);
          })
          .catch((err) => {
            console.log({ err });
          });
      });
    }
  }, [null]);

  const addOrRemoveItemFromArray = (item) => {
    if (programs.includes(item)) {
      setPrograms((prevState) =>
        prevState.filter((existing) => existing.windowName !== item.windowName)
      );
    } else {
      setPrograms((prevState) => [item, ...prevState.slice(0, 2)]);
    }
  };

  function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor((d % 3600) / 60);
    var s = Math.floor((d % 3600) % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    
    return hDisplay + mDisplay + sDisplay;
  }

  const logout = () => {
    setCancelModal(false);
    if(isCancelModal){
      ipcRenderer.send("stop-all", true)
      remove();
      if(ssInterval !== null || appInterval !== null){
        clearInterval(ssInterval);
        clearInterval(appInterval);
      }
      history.replace("/");
      
    }
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>ProdChimp</h1>
        {/* <button onClick={takeSS}>Take SS</button> */}
        <button style={{ width: "50%" }} onClick={() => setCancelModal(true)}>
          Logout
        </button>
        <p>{programs?.windowName}</p>
        <p>{secondsToHms(programs?.idleTime)}</p>
        {/* <p>{isUserActive}</p> */}
      </header>
      {isCancelModal && (
        <div
          className="modal d-block"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div
            className="modal-dialog modal-dialog-centered modal-md delete-modal"
            role="document"
          >
            <div className="modal-content">
              <div className="modal-body">
                {/* <strong>
                  Log-Out
                  <span className="text-red"></span>
                </strong> */}
                <p
                  className="text-secondary"
                  style={{ fontSize: "14px", fontWeight: "600" }}
                >
                  Are you sure that you want to logout?
                  <br />
                </p>
              </div>
              <div className="modal-footer">
                <button
                  type="submit"
                  className="btn btn-primary"
                  onClick={logout}
                >
                  Yes
                </button>
                <button
                  type="reset"
                  className="btn btn-secondary mr-2"
                  data-dismiss="modal"
                  onClick={() => {
                    setCancelModal(false);
                    //setReset(false)
                  }}
                >
                  No
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default App;
