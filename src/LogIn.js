import React, { useEffect, useState } from "react";
import "./LogIn.css";
import { useHistory } from "react-router-dom";
import { handle, isLoggedIn, set, setCookie } from "./authHelpers";
import axios from "axios";
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import toast, { Toaster } from "react-hot-toast";

const { ipcRenderer } = window.require("electron");

// import { createBrowserHistory } from 'history';

function LogIn() {
  const history = useHistory();
  // const history = createBrowserHistory();

  useEffect(() => {
    if (isLoggedIn()) {
      history.push("/app");

    } else {
      history.push("/");
    }
  }, [null]);

    useEffect(() => {
      if (!isLoggedIn()) {
        ipcRenderer.send("stop-all", false);
      }
    }, []);


  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorState, setErrorState] = useState(false);

  async function userLogin(values) {
    axios
      .post("http://localhost:9000/api/users/login", values)
      .then(async (res) => {
        if (res.status === 200) {
          if (res.data.data.token) {
            handle(res.data.data.token);
            history.push("/app");
            setErrorState(false);
          }
          console.log(res)
          if(!res.data.success){
            toast.error(res.data.message,{id:"login error", duration:5000, position: 'top-right'})
          }
        } else {
          setErrorState(true);
        }
      })
      .catch((err) => {
        setErrorState(true);
      });
  }
  return (
    <div className="login-form">
      <Formik
        initialValues={{
          email: '',
          password: '',
        }}
        validationSchema={
          Yup.object().shape({
            email: Yup.string().email('Invalid email').required('Email is Required'),
            password: Yup.string()
              .required('Password is required').matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/, 'Wrong Password'),
          })
        }
        onSubmit={values => {
          userLogin(values);
        }}
      >
        {({ errors, touched }) => (
          <Form>
            <div className="text-center" style={{ marginBottom: "20px", fontSize: "30px", color: "#fff" }}>LOGIN</div>
            {errorState ?
              <div className="alert alert-danger d-flex justify-content-between" onClick={() => setErrorState(false)}>
                <span className="text-danger">Invalid Email and Password</span>
                <button style={{
                  width: "50px",
                  height: "100%",
                  position: "absolute",
                  right: "18px",
                  top: "50%",
                  transform: "translateY(-50%)",
                  margin: "0"
                }} className="btn text-danger">X</button>
              </div>
              : ""}
            <div className="form-group">
              <label>Email address </label>
              <Field name="email" type="email" className={
                "form-control" +
                (errors.email && touched.email
                  ? " is-invalid"
                  : "")
              } />
              <ErrorMessage name="email" component="div"
                className="invalid-feedback"
              />
            </div>
            <div className="form-group">
              <label>Password </label>
              <Field name="password" type="password" className={
                "form-control" +
                (errors.password && touched.password
                  ? " is-invalid"
                  : "")
              } />
              <ErrorMessage name="password" component="div"
                className="invalid-feedback"
              />
            </div>
            <button className="btn btn-success" type="submit">Login</button>
          </Form>
        )}
      </Formik>
      <Toaster/>
    </div>
  );
}

export default LogIn;
