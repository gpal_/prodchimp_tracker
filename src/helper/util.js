export const  upsertReact = (array,element) =>  {
        const i = array.findIndex((_element) => _element.name === element.name);
        if (i > -1) array[i] = element; // (2)
        else array.push(element);
} 

export const isElectron  = () => {
    return !!(window && window.process && window.process.type);
  }