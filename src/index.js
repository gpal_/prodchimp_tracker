import React from "react";
import Main from "./Main";
import "./index.css";
import ReactDOM from 'react-dom/client';

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <>
    <Main />
  </>
);

