module.exports = {
    upsert: (array, element) => {
        const i = array.findIndex((_element) => _element.name === element.name);
        if (i > -1) array[i] = element; // (2)
        else array.push(element);
    },
    checkUserIsActive: (sec) => {
        if (sec >= 60) {
            return true
        } else {
            return false
        }
    },

    /**
  * Conserve aspect ratio of the original region. Useful when shrinking/enlarging
  * images to fit into a certain area.
  *
  * @param {Number} srcWidth width of source image
  * @param {Number} srcHeight height of source image
  * @param {Number} maxWidth maximum available width
  * @param {Number} maxHeight maximum available height
  * @return {Object} { width, height }
  */
    calculateAspectRatioFit: (srcWidth, srcHeight, maxWidth, maxHeight) => {

        var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

        return { width: srcWidth * ratio, height: srcHeight * ratio };
    }


}








// function launchAtStartup() {
//   if (process.platform === "darwin") {
//     app.setLoginItemSettings({
//       openAtLogin: true,
//       openAsHidden: true,
//     });
//   } else {
//     app.setLoginItemSettings({
//       openAtLogin: true,
//       openAsHidden: true,
//       path: updateExe,
//       args: [
//         "--processStart",
//         `"${exeName}"`,
//         "--process-start-args",
//         `"--hidden"`,
//       ],
//     });
//   }
// }

// var autoLauncher = new AutoLaunch({
//   name: "Scalatracker",
// });
// // Checking if autoLaunch is enabled, if not then enabling it.
// autoLauncher
//   .isEnabled()
//   .then(function (isEnabled) {
//     if (isEnabled) return;
//     autoLauncher.enable();
//   })
//   .catch(function (err) {
//     throw err;
//   });

// Stop error
