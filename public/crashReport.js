const { crashReporter } = require('electron');

const host = 'http://localhost:9000/';
  
  crashReporter.start({
    productName: 'YourName',
    companyName: 'YourCompany',
    submitURL: host + 'api/app-crashes',
    uploadToServer: true
  })
