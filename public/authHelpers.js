const mainServerAppUrl = "http://157.245.98.234/api";

export const iss = {
  login: mainServerAppUrl + "/users/login",
};

module.exports = {
  setCookie: (data) => {
    let expires = "";
    if (data.days) {
      let date = new Date();
      date.setTime(date.getTime() + data.days * 24 * 60 * 60 * 1000);
      expires = "; expires=" + date.toUTCString();
    }
    document.cookie = data.name + "=" + (data.value || "") + expires + "; path=/";
  },

  getCookie: (name) => {
    var nameEQ = name + "=";
    var ca = document.cookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == " ") c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
  },

  eraseCookie: (name) => {
    document.cookie = name + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
  },

  deCodePayload: (token) => {
    return JSON.parse(atob(token));
  },
  getPayload: (token) => {
    const payload = token.split(".")[1];
    return deCodePayload(payload);
  },
  getAuthUser: () => {
    const token = getCookie("_token");
    if (token) {
      const payload = getPayload(token);
      return {
        _id: payload._id,
        reportsTo: payload.reportsTo,
        role: payload.role,
        emailId: payload.email,
        organization: payload?.organization,
      };
    }
  },
  isValid: () => {
    const token = getCookie("_token");

    if (token) {
      const payload = getPayload(token);

      if (Date.now() >= payload?.exp * 1000) {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  },

  isLoggedIn: () => {
    return isValid();
  }

}




