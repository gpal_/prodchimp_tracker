const Datastore = require("nedb");
const os = require("os");

module.exports = {
  appDB: () => {
    const db = new Datastore({
      filename: `${os.tmpdir()}/app-data.bin`,
      autoload: true,
      timestampData: true,
      onload: (err) => {
        // console.log("App Data DB this is on load");
      },
    });
    return db;
  },

  appCollection: () => {
    const db = new Datastore({
      filename: `${os.tmpdir()}/app-collection.bin`,
      autoload: true,
      timestampData: true,
      onload: (err) => {
        // console.log("app Collection DB this is on load");
      },
    });
    return db;
  },
};
