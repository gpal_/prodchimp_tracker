const { app, Menu, Tray } = require("electron");


const trayMenu = (currenWindowRunning) => {
let tray;
  currenWindowRunning.on("close", (e) => {
    if (!app.isQuitting) {
      e.preventDefault();
      currenWindowRunning.hide();
    }
    return true;
  });

  const tryIcon = `${__dirname}/icon/tray_icon.png`;
  tray = new Tray(tryIcon);
  tray.on("click", () => {
    if (currenWindowRunning.isVisible() === true) {
      currenWindowRunning.hide();
    } else {
      currenWindowRunning.show();
    }
  });
  tray.on("right-click", () => {
    const contextMenu = Menu.buildFromTemplate([
      {
        label: "Quit",
        click: () => {
          app.isQuitting = true;
          app.quit();
        },
      },
    ]);
    tray.popUpContextMenu(contextMenu);
  });

  app.on("closed", () => {
    currenWindowRunning == null;
  });
};

module.exports = {
  trayMenu,
};
