// Modules to control application life and create native browser window
const {
  app,
  BrowserWindow,
  ipcMain,
  Tray,
  Menu,
  powerMonitor,
  screen,
  desktopCapturer,
} = require("electron");
const path = require("path");
const isDev = require("electron-is-dev");
const url = require("url");
const { trayMenu } = require("./menu");
const fs = require("fs");
const screenshot = require("screenshot-desktop");
const os = require("os");
const axios = require("axios");
var FormDataD = require("form-data");
const { checkUserIsActive, calculateAspectRatioFit } = require("./util");
var Jimp = require("jimp");
const { appDB, appCollection } = require("./db");

const si = require('systeminformation');
const mainUrlWeb = isDev
  ? "http://localhost:9000/api"
  : "http://157.245.98.234/api";
const loadURL = "http://localhost:3000";
let mainWindow = null;
const gotTheLock = app.requestSingleInstanceLock();
const activeWindow = require("active-win");
const { autoUpdater } = require("electron-updater");


function createWindow() {
  const args = process.argv.slice(1),
    serve = args.some((val) => val === "--serve");

  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    autoHideMenuBar: true,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      allowRunningInsecureContent: serve ? true : false,
      enableRemoteModule: true, // true if you want to run 2e2 test or use remote module in renderer context (ie. Angular)
    },
  });

  if (isDev) {
    try {
      require("electron-reload")(__dirname, {
        electron: require(`${__dirname}/../node_modules/electron`),
      });
      mainWindow.webContents.openDevTools();
      mainWindow.loadURL(loadURL);
    } catch (error) {
      console.log(error);
    }
  } else {
    let indexPath = url.format({
      protocol: "file:",
      pathname: path.join(__dirname, "../build/index.html"),
      slashes: true,
    });
    mainWindow.loadURL(indexPath);
  }

  mainWindow.once("ready-to-show", () => {
    mainWindow.show();
  });

  if (isDev) {
    mainWindow.on("closed", () => {
      app.quit();
    });
  } else {
    mainWindow.on("closed", () => (mainWindow = null));
  }
}




function getProductivityValue(selectedWindowName, fn) {
  appCollection().findOne(
    { appAndWebsite: selectedWindowName },
    function (err, doc) {
      if (doc) {
        return fn(doc.Type);
      }
      if (doc === null) {
        return fn(0);
      }
    }
  );

}

function checkFunctionStatus(data) {
  return data
}


ipcMain.on("take-system-info", (reData, userData) => {
  let model = os.cpus();
  si.system().then((result) => {
    si.graphics().then((gpuData) => {
      let data = {
        platform: os.platform(),
        serial: result.serial,
        ram: Math.floor(os.totalmem() / 1e9) + "GB",
        additionalDetails: {
          processor: model[1].model,
          hostName: os.hostname(),
          systemType: os.type(),
          version: os.version(),
          machine: os.arch(),
          manufacturer: result.manufacturer,
          model: result.model,
          version: result.version,
          uuid: result.uuid,
          sku: result.sku,
          gpuDetails: gpuData.controllers[0],
        },
      }
      let config = {
        headers: {
          Authorization: userData.userToken,
        },
      };
      axios
        .post(mainUrlWeb + "/device/store-details", data, config)
        .then(async (res) => {
          try {
            // console.log(res);
          } catch (error) {
            // console.log({ error });
          }
        })
        .catch((err) => {
          // console.log(err);
        });
    });
  });
});

ipcMain.on("call-sync-app-collection", (e, incommingData) => {

  let config = {
    headers: {
      Authorization: incommingData.token,
    },
  };

  appCollection()
    .findOne({})
    .sort({ updatedAt: -1 })
    .limit(1)
    .exec(async (err, docs) => {
      if (docs === null) {
        let data = {
          reqtype: "get-new-data",
        };
        try {
          let newData = await axios.post(
            `${mainUrlWeb}/app-collection-sync`,
            data,
            config
          );
          let appList = [...newData.data];
          appList = appList.map((item) => {
            return {
              uid: item._id,
              isDeleted: item.isDeleted,
              organization: incommingData.organization,
              appAndWebsite: item.appAndWebsite,
              Type: item.Type,
              URL: item.URL,
              createdAt: item.createdAt,
              updatedAt: item.updatedAt,
            };
          })
          appCollection().insert([...appList]);
        } catch (error) {
          console.log(error);
        }
      }

      if (docs) {
        try {

          let data = {
            reqtype: "get-updated-entries",
            selectedTimeStamp: docs.updatedAt,
          };

          let newData = await axios.post(
            `${mainUrlWeb}/app-collection-sync`,
            data,
            config
          );

          let updatedEntries = [...newData.data]

          if (updatedEntries.length > 0) {
            updatedEntries.forEach((item) => {
              let data = {
                uid: item._id,
                isDeleted: item.isDeleted,
                appAndWebsite: item.appAndWebsite,
                Type: item.Type,
                URL: item.URL,
                createdAt: item.createdAt,
                updatedAt: item.updatedAt,
              };
              appCollection().update(
                { uid: item.uid },
                { $set: data },
                { upsert: true },
                (err, doc) => {
                  console.log({ err, doc });
                }
              );
            })

          }

        } catch (error) {
          console.log({ errorOnupdate: error });
        }

      }
    });
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
if (!gotTheLock) {
  app.quit();
} else {
  app.on("second-instance", (event, commandLine, workingDirectory) => {
    // Someone tried to run a second instance, we should focus our window.
    if (mainWindow) {
      if (mainWindow.isMinimized()) mainWindow.restore();
      mainWindow.focus();
    }
  });

  app.whenReady().then(() => {
    createWindow();

    if (!isDev) {
      trayMenu(mainWindow);
    }

    function setSystemStatus(data) {
      let status = data === undefined ? true : data
      return status
    }

    powerMonitor.addListener("suspend", (data) => {
      mainWindow.webContents.send("system-suspend");
    });

    powerMonitor.addListener("resume", (resume) => {
      mainWindow.webContents.send("system-resume");
    });
    powerMonitor.addListener("idle", (resume) => {
      mainWindow.webContents.send("idle");
    });


    ipcMain.on("delete-app-data", function (fn, storData) {
      if (storData) {
        let incommingData = [...storData]
        let selectedIds = [];
        console.log({ incommingData });
        if (incommingData.length > 0) {
          incommingData.map((item, index) => {
            console.log({ item });
            if (item !== null) {
              if (item.hasOwnProperty("incomingId")) {
                selectedIds.push(item.incomingId);
              }
            }
          });

          selectedIds.map((item) => {
            appDB().remove({ _id: item });
          });
          console.log({ removeItem: selectedIds });
        }
      }
      // db.remove({ _id: 'id2' }
    });


    ipcMain.on("stop-all", (e, data) => {
      checkFunctionStatus(data);
    });



    ipcMain.on("track-app", async (reData, userData) => {
      console.log("track-app-on");
      const appUsage = userData;
      if (appUsage.appTracking === true) {
        let timer = powerMonitor.getSystemIdleTime();
        let idle = checkUserIsActive(timer);
        let window = await activeWindow();

        if (window) {
          getProductivityValue(window.owner.name, function (data) {
            let windowData = {
              windowClass: window.owner.name,
              windowName: window.title,
              idleTime: idle,
              type: data,
            };

            appDB()
              .findOne({})
              .sort({ createdAt: -1 })
              .limit(1)
              .exec((err, docs) => {
                let selectedAppData = docs;
                if (err) {
                  new Error(err);
                }

                if (selectedAppData === null) {
                  appDB().insert(windowData);
                  return true;
                }

                if (
                  selectedAppData?.windowName !== windowData["windowName"] &&
                  selectedAppData?.windowClass !== windowData["windowClass"]
                ) {
                  appDB().insert(windowData);
                  return true;
                }

                if (
                  selectedAppData.windowName === windowData.windowName &&
                  selectedAppData.windowClass === windowData.windowClass
                ) {
                  let data = {
                    duration: Number(docs.duration) + 5,
                    updatedAt: new Date().toISOString(),
                  };
                  appDB().update(
                    { _id: selectedAppData._id },
                    {
                      $set: data,
                    },
                    { upsert: true }
                  );

                  return true;
                }
              });
          });

        }
      }

    });

    ipcMain.on("take-ss", async (reData, userData) => {
      if (mainWindow) {
        screenshot
          .all()
          .then(async (imgs) => {
            let localImgDir = `${os.tmpdir()}/track-images`;

            if (!fs.existsSync(localImgDir)) {
              fs.mkdirSync(localImgDir);
            }

            // call function getType
            let window1 = await activeWindow();

            getProductivityValue(window1.owner.name, (data) => {
              imgs.map((imgData, i) => {
                let date = new Date().getTime();
                let iData = {
                  imgName: `${data}_${window1.owner.name}_${window1.title}_${date}.png`,
                  imgTag: data,
                };

                fs.writeFileSync(
                  `${localImgDir}/orignal-${i}.png`,
                  imgData,
                  (err, data) => {
                    if (data) {
                      console.log("file is created");
                    }
                  }
                );

                Jimp.read(`${localImgDir}/orignal-${i}.png`).then(
                  async (lenna) => {
                    let wnH = calculateAspectRatioFit(
                      lenna.getWidth(),
                      lenna.getHeight(),
                      1920,
                      1080
                    );
                    let img = lenna
                      .resize(wnH.width, wnH.height)
                      .quality(10)
                      .write(`${localImgDir}/compresed-ss-${i}.png`);

                    fs.unlink(
                      `${localImgDir}/orignal-${i}.png`,
                      function (err) {
                        if (err) return console.error(err);
                      }
                    );

                    fs.readdir(localImgDir, (err, files) => {
                      files.forEach((file) => {
                        fs.readFile(
                          `${localImgDir}/${file}`,
                          { encoding: "base64" },
                          (err, data) => {
                            if (err) {
                              return 0;
                            }

                            if (data) {
                              const buffer = Buffer.from(
                                data.replace(/^data:image\/\w+;base64,/, ""),
                                "base64"
                              );
                              let config = {
                                headers: {
                                  Authorization: userData.userToken,
                                },
                              };

                              axios
                                .post(mainUrlWeb + "/s3Url", iData, config)
                                .then((res) => {
                                  let options = {
                                    headers: {
                                      "Content-Type": "image/jpeg",
                                    },
                                  };

                                  console.log('aws', res.data.url);
                                  axios
                                    .put(res.data.url, buffer, options)
                                    .then((upload) => {
                                      console.log("aws", upload);

                                      fs.unlink(
                                        `${localImgDir}/compresed-ss-${i}.png`,
                                        function (err) {
                                          if (err) return console.error(err);
                                        }
                                      );
                                    })
                                    .catch((err) => {
                                      console.log("error from upload", err);
                                    });
                                })
                                .catch((err) => {
                                  console.log(err);
                                });
                            }
                          }
                        );
                      });
                    });
                  }
                );
              }); //map end here;
            });
          })
          .catch((e) => {
            mainWindow.webContents.send("screen-check", {
              data: "SS taken event from BE" + e,
            });
          });

      }
    });


    setInterval(() => {
      appDB().find({}, function (err, appData) {
        let data = JSON.stringify(appData);

        mainWindow.webContents.send("active-window-check", {
          data: data,
        });
      });
    }, 60000);

    setInterval(() => {
      // let data = {
      //   name : "Gaurav Palaspagar"
      // }
      // mainWindow.webContents.send("update-chek",data)
      autoUpdater.checkForUpdatesAndNotify()
      .then((res) => {
        mainWindow.webContents.send("update-chek",res)
        console.log({res});
      })
      .catch((err) => {
        console.log({err});
        mainWindow.webContents.send("update-chek-err",err)
        
      })
      console.log("checking for update...");
    }, 1000);


  });


  app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
      app.quit();
    }
  });

  app.on("activate", () => {
    if (mainWindow === null) {
      createWindow();
    }
  });

  app.on("browser-window-focus", () => {
    // Your code
    // console.log("code");
    // activeWindows()
    //   .getActiveWindow()
    //   .then((sresult) => {
    //     console.log(sresult);
    //   });
  });

  app.allowRendererProcessReuse = true;
}

function currentTimeMinusJobInterval() {
  let now = new Date();
  // Begin date is always BACKGROUND_JOB_INTERVAL before current date
  now.setMilliseconds(now.getMilliseconds() - 3000);
  return now;
}

function isSameItems(item1, item2) {
  if (
    item1 &&
    item2 &&
    item1.app === item2.app &&
    item1.title === item2.title
  ) {
    return true;
  }

  return false;
}

function storeToLocalStorage() { }


autoUpdater.on("update-available", (info) => {
  console.log("Update available");
});

autoUpdater.on("update-not-available", (info) => {
  console.log("Update not available");
});

autoUpdater.on("error", (err) => {
  console.log("Error in auto-updater. " + err);
});

autoUpdater.on("download-progress", (progressTrack) => {
  console.log("Download in progress");
  console.log(progressTrack);
});

autoUpdater.on("update-downloaded", (info) => {
  console.log("update-downloaded");
  autoUpdater.NotifyQuitAndInstall();
});

autoUpdater.on("update-downloaded", (ev, info) => {
  // Wait 5 seconds, then quit and install
  // In your application, you don't need to wait 5 seconds.
  // You could call autoUpdater.quitAndInstall(); immediately
  setTimeout(function () {
    autoUpdater.quitAndInstall();
  }, 5000);
});