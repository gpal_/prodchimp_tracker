const CryptoJS = require('crypto-js')
const fs = require('fs')
const os = require("os");
let password = "12345678";
module.exports = {
    /**
     * @param  {} filePath
     */
    encryptionFile: (filePath) => {
        
        // read data from file
        fs.readFile(filePath, { encoding: "utf8" }, (err, data) => {
            if(err) return false
            let encryptText = CryptoJS.AES.encrypt(data, password).toString();

            fs.writeFile(
              filePath,
              encryptText,
              { encoding: "utf-8" },
              (err) => new Error(err)
            );
        });
    },
    decryptionFile: (filePath) => {

        fs.readFile(filePath, { encoding: "utf-8" }, (err, fdata) => {
            if (err) {
                return 0;
            }

            if (fdata) {
                var decryptText = CryptoJS.AES.decrypt(
                  fdata,
                  password
                ).toString(CryptoJS.enc.Utf8);

                fs.writeFile(
                  filePath,
                  decryptText,
                  { encoding: "utf-8" },
                  (err) => new Error(err)
                );
            }
        });


    }

}